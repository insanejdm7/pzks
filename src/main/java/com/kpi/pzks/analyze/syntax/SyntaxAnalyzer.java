package com.kpi.pzks.analyze.syntax;

import com.kpi.pzks.State;
import com.kpi.pzks.analyze.Analyzer;
import com.kpi.pzks.exception.syntax.MissingTokenException;
import com.kpi.pzks.exception.syntax.UnexpectedTokenException;
import com.kpi.pzks.token.Token;

import java.util.*;

/**
 * Created by insane on 24.09.14.
 */
public class SyntaxAnalyzer
        implements Analyzer {

    private final List<Token> tokens;
    private final Set<String> allowedFunNames;

    public SyntaxAnalyzer(List<Token> tokens, Set<String> allowedFunNames) {
        this.tokens = new ArrayList<>(tokens);
        this.allowedFunNames = allowedFunNames;
    }

    @Override
    public void process()
            throws Exception {
        if (tokens.size() == 1 && tokens.get(0).getType() == State.START) {
            return;
        }
        castVarsToFuns();
        castOpsToSigns();
        checkSequence();
        validateBraces();
    }

    private void castVarsToFuns() {
        for (int index = 1; index < tokens.size(); index++) {
            if (getTokenByIndex(index).getType() == State.VAR && allowedFunNames.contains(getTokenByIndex(index).getDescription())
                    && getTokenByIndex(index + 1) != null && getTokenByIndex(index + 1).getType() == State.OPEN_BRACE) {
                getTokenByIndex(index++).setType(State.FUN);
            }
        }
    }

    private void castOpsToSigns() {
        for (int index = 1; index < tokens.size(); index++) {
            if (getTokenByIndex(index - 1) != null && getTokenByIndex(index - 1).getType().nextPossibleStates().contains(State.SIGN) && getTokenByIndex(index).getType() == State.OPERATION && State.SIGN.isStart(getTokenByIndex(index).getDescription().charAt(0)) && getTokenByIndex(index + 1) != null && State.SIGN.nextPossibleStates().contains(getTokenByIndex(index + 1).getType())) {
                getTokenByIndex(index++).setType(State.SIGN);
            }
        }
    }

    private void checkSequence()
            throws Exception {
        Token currentToken;
        Token nextToken;
        Set<State> nextPossibleTokens;
        for (int index = 0; index < tokens.size(); index++) {
            currentToken = tokens.get(index);
            nextPossibleTokens = currentToken.getType().nextPossibleStates();
            nextToken = getTokenByIndex(index + 1);
            if (nextToken != null) {
                if (!nextPossibleTokens.contains(nextToken.getType())) {
                    throw new UnexpectedTokenException(nextToken, currentToken);
                }
            } else if (currentToken.getType() != State.END) {
                throw new MissingTokenException(currentToken, currentToken.getType().nextPossibleStates());
            }
        }
    }

    private void validateBraces()
            throws Exception {
        final Stack<Token> stack = new Stack<>();
        final List<Token> braces = tokens.stream()
                .filter(token -> token.getType() == State.OPEN_BRACE || token.getType() == State.CLOSE_BRACE)
                .collect(LinkedList::new, LinkedList::add, LinkedList::addAll);
        for (Token token : braces) {
            if (token.getType() == State.OPEN_BRACE) {
                stack.push(token);
            } else if (!stack.isEmpty()) {
                stack.pop();
            } else {
                throw new UnexpectedTokenException(token);
            }
        }
        if (!stack.isEmpty()) {
            throw new MissingTokenException(stack.peek(), stack.peek().getType().nextPossibleStates());
        }
    }

    private Token getTokenByIndex(int index) {
        if (index >= tokens.size()) {
            return null;
        }
        return tokens.get(index);
    }

    public List<Token> getTokens() {
        return tokens;
    }
}

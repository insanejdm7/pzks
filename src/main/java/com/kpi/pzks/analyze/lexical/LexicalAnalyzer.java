package com.kpi.pzks.analyze.lexical;

import com.kpi.pzks.State;
import com.kpi.pzks.analyze.Analyzer;
import com.kpi.pzks.exception.lexical.UnexpectedCharacterException;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.token.builder.*;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.PushbackInputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by insane on 23.09.14.
 */
public class LexicalAnalyzer implements Analyzer {

    private final PushbackInputStream stream;

    private final List<Token> tokens;

    public LexicalAnalyzer(String str) {
        if (str.isEmpty()) {
            str = " ";
        }
        stream = new PushbackInputStream(new BufferedInputStream(new ByteArrayInputStream(str.getBytes())), str.length());
        tokens = new LinkedList<>();
    }

    @Override
    public void process() throws Exception {
        long pos = 0;
        tokens.add(new GeneralToken(State.START, "", 0));
        int b;
        for (; stream.available() > 0; ) {
            b = stream.read();
            if (State.VAR.isStart(b)) {
                stream.unread(b);
                tokens.add(new VariableTokenBuilder(stream, pos).build());
                pos += tokens.get(tokens.size() - 1).getDescription().length();
            } else if (State.CONST.isStart(b)) {
                stream.unread(b);
                tokens.add(new ConstTokenBuilder(stream, pos).build());
                pos += tokens.get(tokens.size() - 1).getDescription().length();
            } else if (State.OPEN_BRACE.isStart(b)) {
                stream.unread(b);
                tokens.add(new OpenBraceTokenBuilder(stream, pos).build());
                pos++;
            } else if (State.CLOSE_BRACE.isStart(b)) {
                stream.unread(b);
                tokens.add(new CloseBraceTokenBuilder(stream, pos).build());
                pos++;
            } else if (State.OPERATION.isStart(b)) {
                stream.unread(b);
                tokens.add(new OperationTokenBuilder(stream, pos).build());
                pos++;
            } else if (State.END.isStart(b)) {
                stream.unread(b);
                tokens.add(new EndTokenBuilder(stream, pos).build());
                pos++;
            } else if (State.START.isStart(b)) {
                pos++;
            } else {
                throw new UnexpectedCharacterException(Character.toChars(b)[0], pos);
            }
        }
    }

    public List<Token> getTokens() {
        return tokens;
    }
}

package com.kpi.pzks.analyze;

/**
 * Created by insane on 24.09.14.
 */
public interface Analyzer {

    void process() throws Exception;

}

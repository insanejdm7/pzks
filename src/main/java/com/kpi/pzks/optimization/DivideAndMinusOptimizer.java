package com.kpi.pzks.optimization;

import com.kpi.pzks.State;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.util.Pair;

import java.util.*;

/**
 * Created by insane on 18.10.14.
 */
public class DivideAndMinusOptimizer extends TokenListOptimizer {

    private static final Pair<String, String> PAIR1 = new Pair<>("/", "*");
    private static final Pair<String, String> PAIR2 = new Pair<>("-", "+");

    public DivideAndMinusOptimizer(List<Token> tokens) {
        super(tokens);
    }

    @Override
    public List<Token> optimize() {

        Pair<String, String> activePair;
        boolean converted;
        while (true) {
            final Map<Token, Integer> filtered = filter();
            converted = false;
            for (final Map.Entry<Token, Integer> entry : filtered.entrySet()) {
                if (PAIR1.getK().equals(entry.getKey().getDescription())) {
                    activePair = PAIR1;
                } else if (PAIR2.getK().equals(entry.getKey().getDescription())) {
                    activePair = PAIR2;
                } else {
                    continue;
                }
                final int len = countOperations(entry.getValue(), activePair.getK());
                if (len < 2) {
                    continue;
                }
                convertOperations(activePair, entry.getValue(), len);
                converted = true;
                break;
            }
            if (!converted) {
                break;
            }
        }
        return tokens;
    }

    private Map<Token, Integer> filter() {
        final Map<Token, Integer> result = new LinkedHashMap<>();
        for (int index = 0; index < tokens.size(); index++) {
            final Token token = tokens.get(index);
            if (State.OPERATION.equals(token.getType()) || State.OPEN_BRACE.equals(token.getType()) || State.CLOSE_BRACE.equals(token.getType())) {
                result.put(token, index);
            }
        }
        return result;
    }

    /**
     * @param start  start pos from original token list
     * @param length number of devide operations
     */
    private void convertOperations(Pair<String, String> pair, int start, int length) {
        final Token openBrace = new GeneralToken(State.OPEN_BRACE, "(", Integer.MAX_VALUE);
        tokens.add(start + 1, openBrace);
        int done = 1;
        int index = start + 2;
        for (; done < length; index++) {
            final Token current = tokens.get(index);
            if (State.OPERATION.equals(current.getType()) && pair.getK().equals(current.getDescription())) {
                current.setDescription(pair.getV());
                done++;
            } else if (State.OPEN_BRACE.equals(current.getType())) {
                index = skipBraces(index);
            }
            if (index == tokens.size() - 1) {
                index--;// = tokens.size()-2;
                break;
            }
        }
        final Token closeBrace = new GeneralToken(State.CLOSE_BRACE, ")", Integer.MAX_VALUE);
        for (; index < tokens.size(); index++) {
            final Token current = tokens.get(index);
            if (State.VAR.equals(current.getType()) || State.CONST.equals(current.getType()) || State.FUN.equals(current.getType())) {
                tokens.add((PAIR2 == pair ? skipHighPriorityOperations(index) : index) + 1, closeBrace);
                return;
            } else if (State.OPEN_BRACE.equals(current.getType())) {
                index = skipBraces(index);
                tokens.add(index + 1, closeBrace);
                return;
            } else {
                tokens.add(index + 1, closeBrace);
                return;
            }
        }
    }

    private int countOperations(int start, String operation) {
        int counter = 0;
        for (int index = start; index < tokens.size(); index++) {
            final Token token = tokens.get(index);
            switch (token.getType()) {
                case OPERATION:
                    if (operation.equals(token.getDescription())) {
                        counter++;
                        //kostul
                        Optional<Token> optional = findNextOperation(index);
                        if (optional.isPresent() && (PAIR1.getK().equals(optional.get().getDescription()) || PAIR1.getV().equals(optional.get().getDescription())) && PAIR2.getK().equals(operation)) {
                            index = skipHighPriorityOperations(index + 1);
                        }
                    } else {
                        return counter;
                    }
                    break;
                case OPEN_BRACE:
                    index = skipBraces(index);
                    continue;
                case CLOSE_BRACE:
                    return counter;
                default:
                    continue;
            }
        }
        return counter;
    }

    private int skipHighPriorityOperations(int start) {
        for (int index = start; index < tokens.size(); index++) {
            final Token token = tokens.get(index);
            if (State.OPEN_BRACE.equals(token.getType())) {
                index = skipBraces(index);
            } else if (State.OPERATION.equals(token.getType())) {
                if (PAIR2.getK().equals(token.getDescription()) || PAIR2.getV().equals(token.getDescription())) {
                    return index - 1;
                }
            }
        }
        return tokens.size() - 2;
    }

    private int skipBraces(int start) {
        final Stack<Token> stack = new Stack<>();
        for (int index = start; index < tokens.size(); index++) {
            final Token token = tokens.get(index);
            switch (token.getType()) {
                case OPEN_BRACE:
                    stack.push(token);
                    break;
                case CLOSE_BRACE:
                    stack.pop();
                    if (stack.isEmpty()) {
                        return index;
                    }
                    break;
                default:
                    continue;
            }
        }
        return tokens.size();
    }

    private Optional<Token> findNextOperation(int tokenIndex) {
        return tokens.stream().skip(tokenIndex + 1).filter(token -> State.OPERATION.equals(token.getType())).findFirst();
    }
}

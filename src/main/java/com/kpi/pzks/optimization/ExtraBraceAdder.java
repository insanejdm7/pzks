package com.kpi.pzks.optimization;

import com.kpi.pzks.Operation;
import com.kpi.pzks.State;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * bottom-up shift-reduce parser
 * <p>
 * Created by insane on 31.10.14.
 */
public class ExtraBraceAdder
        extends TokenListOptimizer {

    private final Stack<LinkedList<Token>> operands;
    private final Stack<Token> operations;

    public ExtraBraceAdder(List<Token> tokens) {
        super(tokens);
        operands = new Stack<>();
        operations = new Stack<>();
    }

    /*
     * here would be better to use 'visitor' but as tokens differs in Enum (not in Class) it is impossible
     */
    @Override
    public List<Token> optimize() {
        for (int index = 0; index < tokens.size(); index++) {
            final Token token = tokens.get(index);
            switch (token.getType()) {
                case FUN:
                    // TODO optimize FUNS before loop in recursive way
                    break;
                case VAR:
                case CONST:
                    if (compare2TailOperations() > 0) {
                        LinkedList<Token> newStackHead = new LinkedList<>();
                        newStackHead.offer(token);
                        operands.push(newStackHead);
                    } else {
                        Token lastStackOp = operations.pop();
                        Token relocateOp = operations.pop();
                        operations.push(lastStackOp);
                        merge2LastOperands(relocateOp);
                        wrapLastOperandsIntoBraces();
                    }
                    break;
                case OPERATION:
                    for (; ; ) {
                        if (compare2TailOperations(token) > 0) {
                            operations.push(token);
                            break;
                        } else {
                            merge2LastOperands(operations.pop());
                            wrapLastOperandsIntoBraces();
                        }
                    }
                    break;
                case OPEN_BRACE:
                    operations.push(token);
                    break;
                case CLOSE_BRACE:
                    processCloseBrace();
                    break;
            }
        }
        finishProcessing();
        return operands.pop();
    }

    private void wrapLastOperandsIntoBraces() {
        wrapLastOperandsIntoBraces(new GeneralToken(State.OPEN_BRACE, "(", Integer.MAX_VALUE));
    }

    private void wrapLastOperandsIntoBraces(Token openBrace) {
        LinkedList<Token> lastOperands = operands.peek();
        lastOperands.addFirst(openBrace);
        lastOperands.offer(new GeneralToken(State.CLOSE_BRACE, ")", Integer.MAX_VALUE));
    }

    private void merge2LastOperands() {
        merge2LastOperands(operations.pop());
    }

    private void merge2LastOperands(Token operation) {
        LinkedList<Token> secondOperand = operands.pop();
        LinkedList<Token> firstOperand = operands.peek();
        firstOperand.offer(operation);
        firstOperand.addAll(secondOperand);
    }

    private void processCloseBrace() {
        Token poppedOp;
        for (poppedOp = operations.pop(); !State.OPEN_BRACE.equals(poppedOp.getType()); poppedOp = operations.pop()) {
            merge2LastOperands(poppedOp);
        }
        wrapLastOperandsIntoBraces(poppedOp);
    }

    private void finishProcessing() {
        for (; operands.size() > 1; ) {
            merge2LastOperands();
            wrapLastOperandsIntoBraces();
        }
    }

    private int compare2TailOperations(Token lastToken) {
        Token lastStackOp;
        try {
            lastStackOp = operations.peek();
            return Operation.getOperation(lastToken.getDescription())
                    .getPriority()
                    .compareTo(Operation.getOperation(lastStackOp.getDescription()).getPriority());
        } catch (EmptyStackException | IllegalArgumentException e) {
            return 1;
        }
    }

    private int compare2TailOperations() {
        Token lastOp = null;
        Token beforeLastOp;
        try {
            lastOp = operations.pop();
            beforeLastOp = operations.peek();
            final Operation last = Operation.getOperation(lastOp.getDescription());
            final Operation beforeLast = Operation.getOperation(beforeLastOp.getDescription());
            final int result = last.getPriority().compareTo(beforeLast.getPriority());
            operations.push(lastOp);
            return result;
        } catch (EmptyStackException | IllegalArgumentException e) {
            if (lastOp != null) {
                operations.push(lastOp);
            }
            return 1;
        }
    }
}

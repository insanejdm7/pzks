package com.kpi.pzks.optimization;

import com.kpi.pzks.token.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by insane on 18.10.14.
 */
public abstract class TokenListOptimizer implements Optimizer<List<Token>> {

    protected final List<Token> tokens;

    public TokenListOptimizer(List<Token> tokens) {
        this.tokens = new ArrayList<>(tokens);
    }
}

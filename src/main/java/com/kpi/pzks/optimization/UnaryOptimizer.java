package com.kpi.pzks.optimization;

import com.kpi.pzks.State;
import com.kpi.pzks.token.Token;

import java.util.List;

/**
 * Created by insane on 28.10.14.
 */
public class UnaryOptimizer extends TokenListOptimizer {
    public UnaryOptimizer(List<Token> tokens) {
        super(tokens);
    }

    @Override
    public List<Token> optimize() {
        for (int index = 0; index < tokens.size(); index++) {
            final Token token = tokens.get(index);
            if (!State.SIGN.equals(token.getType())) {
                continue;
            }
            markAsUnary(tokens.get(index + 1));
            tokens.remove(index--);
        }
        return tokens;
    }

    private void markAsUnary(Token token) {
        token.setDescription("-" + token.getDescription());
    }
}

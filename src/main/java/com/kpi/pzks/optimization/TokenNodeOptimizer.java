package com.kpi.pzks.optimization;

import com.kpi.pzks.tree.TokenNode;

/**
 * Created by insane on 04.11.14.
 */
public abstract class TokenNodeOptimizer implements Optimizer<TokenNode> {

    protected TokenNode root;

    public TokenNodeOptimizer(TokenNode tokenNode) {
        root = tokenNode;
    }

}

package com.kpi.pzks.optimization;

/**
 * Created by insane on 04.11.14.
 */
public interface Optimizer<T> {

    public T optimize();
}

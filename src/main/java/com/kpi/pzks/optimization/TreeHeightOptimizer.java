package com.kpi.pzks.optimization;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;
import com.kpi.pzks.tree.TokenNode;
import com.kpi.pzks.tree.walk.Walker;

/**
 * Created by insane on 04.11.14.
 */
public class TreeHeightOptimizer
    extends TokenNodeOptimizer {

    private Walker<BinaryNode<Token>> walker;

    public TreeHeightOptimizer(TokenNode tokenNode, Walker<BinaryNode<Token>> walker) {
        super(tokenNode);
        this.walker = walker;
    }

    @Override
    public TokenNode optimize() {
        walker.walk(root);
        for (; root.getParent() != null;) {
            root = (TokenNode) root.getParent();
        }
        return root;
    }

}

package com.kpi.pzks;

import com.google.common.collect.Sets;

import java.util.Collections;
import java.util.Set;

/**
 * Created by insane on 23.09.14.
 */
public enum State {
    START {
        @Override
        public boolean isStart(int b) {
            return b == ' ' || b == '\t';
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(OPEN_BRACE, VAR, CONST, FUN, END, SIGN));
        }
    },
    OPEN_BRACE {
        @Override
        public boolean isStart(int b) {
            return b == '(';
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(VAR, CONST, OPEN_BRACE, FUN, SIGN));
        }
    },
    CLOSE_BRACE {
        @Override
        public boolean isStart(int b) {
            return b == ')';
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(END, OPERATION, CLOSE_BRACE));
        }
    },
    VAR {
        @Override
        public boolean isStart(int b) {
            return (b >= 65 && b <= 90 /*upper case*/) || (b >= 97 && b <= 122 /*lower case*/) || b == 95 /*underline*/;
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(OPERATION, CLOSE_BRACE, END));
        }
    },
    FUN {
        @Override
        public boolean isStart(int b) {
            return VAR.isStart(b);
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(OPEN_BRACE));
        }
    },
    CONST {
        @Override
        public boolean isStart(int b) {
            return b >= 48 && b <= 57 /*number*/;
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(OPERATION, CLOSE_BRACE, END));
        }
    },
    OPERATION {
        @Override
        public boolean isStart(int b) {
            return b == '+' || b == '-' || b == '*' || b == '/';
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(OPEN_BRACE, CONST, VAR, FUN, SIGN));
        }
    },
    SIGN {
        @Override
        public boolean isStart(int b) {
            return b == '-';
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet(FUN, CONST, VAR));
        }
    },
    END {
        @Override
        public boolean isStart(int b) {
            return b == ';';
        }

        @Override
        public Set<State> nextPossibleStates() {
            return Collections.unmodifiableSet(Sets.newHashSet());
        }
    },;

    public abstract boolean isStart(int b);

    public abstract Set<State> nextPossibleStates();

}

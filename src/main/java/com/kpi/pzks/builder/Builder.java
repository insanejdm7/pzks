package com.kpi.pzks.builder;

/**
 * Created by insane on 26.10.14.
 */
public interface Builder<T> {

    T build() throws Exception;

}

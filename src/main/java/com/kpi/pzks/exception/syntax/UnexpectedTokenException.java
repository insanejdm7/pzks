package com.kpi.pzks.exception.syntax;

import com.kpi.pzks.token.Token;

/**
 * Created by insane on 25.09.14.
 */
public class UnexpectedTokenException
    extends Exception {

    public UnexpectedTokenException(Token currentToken, Token prevToken) {
        super("Unexpected token \'" + currentToken + "\' found after \'" + prevToken);
    }

    public UnexpectedTokenException(Token currentToken) {
        super("Unexpected token \'" + currentToken + "\' found");
    }

}

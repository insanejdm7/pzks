package com.kpi.pzks.exception.syntax;

import com.kpi.pzks.State;
import com.kpi.pzks.token.Token;

import java.util.Collection;

/**
 * Created by andriiro on 9/25/2014.
 */
public class MissingTokenException
    extends Exception {

    public MissingTokenException(Token currentToken, State missingState) {
        super("Missing token \'" + missingState + "\' after token \'" + currentToken + '\'');
    }

    public MissingTokenException(Token currentToken, Collection<State> missingStates) {
        super("Missing one of tokens \'" + missingStates + "\' after token \'" + currentToken + '\'');
    }
}

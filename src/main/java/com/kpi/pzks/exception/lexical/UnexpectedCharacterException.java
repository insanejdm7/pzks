package com.kpi.pzks.exception.lexical;

/**
 * Created by insane on 24.09.14.
 */
public class UnexpectedCharacterException extends ParseException {

    private final char c;
    private final long pos;

    public UnexpectedCharacterException(char c, long pos) {
        super("Unexpected char \'" + c + "\' found at position " + pos);
        this.c = c;
        this.pos = pos;
    }

    public char getC() {
        return c;
    }

    public long getPos() {
        return pos;
    }
}

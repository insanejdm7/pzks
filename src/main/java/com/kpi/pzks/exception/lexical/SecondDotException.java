package com.kpi.pzks.exception.lexical;

/**
 * Created by insane on 24.09.14.
 */
public class SecondDotException extends ParseException {

    private final String token;
    private final long pos;


    public SecondDotException(String token, long pos) {
        super("Unexpected dot in token \'" + token + "\' at position " + pos);
        this.token = token;
        this.pos = pos;
    }

    public String getToken() {
        return token;
    }

    public long getPos() {
        return pos;
    }
}

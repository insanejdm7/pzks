package com.kpi.pzks.exception.lexical;

/**
 * Created by insane on 23.09.14.
 */
public class ParseException extends Exception {

    public ParseException(String message) {
        super(message);
    }

}

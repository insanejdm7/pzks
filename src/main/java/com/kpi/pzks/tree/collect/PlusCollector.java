package com.kpi.pzks.tree.collect;

import com.kpi.pzks.Operation;
import com.kpi.pzks.State;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;
import com.kpi.pzks.tree.edit.Editor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by insane on 06.11.14.
 */

/**
 * Don't use such difficult optimization Not tested
 */

public class PlusCollector
    implements Collector<BinaryNode<Token>> {

    private List<BinaryNode<Token>> tokensToMerge;

    public PlusCollector() {
        tokensToMerge = new LinkedList<>();
    }

    @Override
    public void collect(BinaryNode<Token> tokenNode) {
        if (tokenNode.getLeft() == null)
            return;
        Token leftToken = tokenNode.getLeft().getElement();
        if (leftToken.getType() != State.OPERATION)
            return;
        Operation tokenOperation = Operation.getOperation(tokenNode.getElement().getDescription());
        if (tokenOperation != Operation.PLUS) {
            return;
        }
        tokensToMerge.add(tokenNode);
    }

    @Override
    public void edit(Editor<BinaryNode<Token>> t) {
        t.edit(tokensToMerge);
    }

    @Override
    public void reset() {
        tokensToMerge.clear();
    }
}

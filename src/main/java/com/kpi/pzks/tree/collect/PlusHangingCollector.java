package com.kpi.pzks.tree.collect;

import com.kpi.pzks.Operation;
import com.kpi.pzks.State;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;
import com.kpi.pzks.tree.edit.Editor;

import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by andriiro on 05.11.14.
 */
public class PlusHangingCollector
    implements Collector<BinaryNode<Token>> {

    // TODO change to list of tokens
    private Map<Operation, List<BinaryNode<Token>>> map = new EnumMap<>(Operation.class);

    @Override
    public void collect(BinaryNode<Token> tokenNode) {
        if (tokenNode.getRight() == null)
            return;
        if (tokenNode.getLeft() == null)
            return;
        Token rightToken = tokenNode.getRight().getElement();
        if (rightToken.getType() != State.CONST && rightToken.getType() != State.VAR)
            return;
        Token leftToken = tokenNode.getLeft().getElement();
        if (leftToken.getType() != State.OPERATION)
            return;
        Operation tokenOperation = Operation.getOperation(tokenNode.getElement().getDescription());
        if (tokenOperation != Operation.PLUS) {
            return;
        }
        if (getTokenNodes(tokenOperation).contains(tokenNode)) {
            return;
        }
        getTokenNodes(tokenOperation).add(tokenNode);
    }

    private List<BinaryNode<Token>> getTokenNodes(Operation operation) {
        List<BinaryNode<Token>> result = map.get(operation);
        if (result == null) {
            result = new LinkedList<>();
            map.put(operation, result);
        }
        return result;
    }

    @Override
    public void edit(Editor<BinaryNode<Token>> t) {
        t.edit(getTokenNodes(Operation.PLUS));
    }

    @Override
    public void reset() {
        map.clear();
    }
}

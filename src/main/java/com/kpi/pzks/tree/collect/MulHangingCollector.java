package com.kpi.pzks.tree.collect;

import com.kpi.pzks.Operation;
import com.kpi.pzks.State;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;
import com.kpi.pzks.tree.edit.Editor;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by andriiro on 05.11.14.
 */
public class MulHangingCollector
    implements Collector<BinaryNode<Token>> {

    private List<List<BinaryNode<Token>>> list = new LinkedList<>();

    public MulHangingCollector() {
        list = new LinkedList<>();
        list.add(new LinkedList<>());
    }

    @Override
    public void collect(BinaryNode<Token> tokenNode) {
        if (tokenNode.getRight() == null)
            return;
        if (tokenNode.getLeft() == null)
            return;
        Token rightToken = tokenNode.getRight().getElement();
        if (rightToken.getType() != State.CONST && rightToken.getType() != State.VAR)
            return;
        Token leftToken = tokenNode.getLeft().getElement();
        if (leftToken.getType() != State.OPERATION)
            return;
        Operation tokenOperation = Operation.getOperation(tokenNode.getElement().getDescription());
        if (tokenOperation != Operation.MUL) {
            return;
        }
        if (list.size() == 0) {
            return;
        }
        List<BinaryNode<Token>> lastList = list.get(list.size() - 1);
        if (lastList.isEmpty() || tokenNode.getLeft() == lastList.get(lastList.size() - 1)) {
            lastList.add(tokenNode);
        } else {
            lastList = new LinkedList<>();
            lastList.add(tokenNode);
            list.add(lastList);
        }

        // getTokenNodes(tokenOperation).add(tokenNode);
    }

    // private List<BinaryNode<Token>> getTokenNodes(Operation operation) {
    // List<BinaryNode<Token>> result = list.get(operation);
    // if (result == null) {
    // result = new LinkedList<>();
    // list.put(operation, result);
    // }
    // return result;
    // }

    @Override
    public void edit(Editor<BinaryNode<Token>> t) {
        for (List<BinaryNode<Token>> tokens : list) {
            t.edit(tokens);
        }
        // t.edit(getTokenNodes(Operation.PLUS));
    }

    @Override
    public void reset() {
        list.clear();
    }
}

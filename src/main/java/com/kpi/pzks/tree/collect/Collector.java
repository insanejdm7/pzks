package com.kpi.pzks.tree.collect;

import com.kpi.pzks.tree.edit.Editor;

/**
 * Created by andriiro on 05.11.14.
 */
public interface Collector<T> {

    void collect(T t);

    void edit(Editor<T> t);

    void reset();

}

package com.kpi.pzks.tree.edit;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;

import java.util.List;

/**
 * Created by andriiro on 05.11.14.
 */

/**
 * Not testes. Don't use this class
 */

public class PlusEditor
        extends TokenNodeEditor {

    @Override
    public void edit(List<? extends BinaryNode<Token>> list) {
        for (; list.size() > 1; ) {
            mergeNodes(list.remove(0), list.remove(0));
        }
    }

    private void mergeNodes(BinaryNode<Token> bottom, BinaryNode<Token> top) {
        bottom.getRight().setParent(top);
        top.setLeft(bottom.getRight());

        top.getParent().setLeft(bottom);
        bottom.setParent(top.getParent());

        bottom.setRight(top);
        top.setParent(bottom);


//        top.getLeft().setParent(top.getParent());
//        top.getParent().setLeft(top.getLeft());
//
//        bottom.getRight().setParent(top);
//        top.setLeft(bottom.getRight());
//
//        bottom.setRight(top);
//        top.setParent(bottom);
    }
}

package com.kpi.pzks.tree.edit;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;

import java.util.List;

/**
 * Created by andriiro on 05.11.14.
 */
public class PlusHangingEditor
        extends TokenNodeEditor {

    @Override
    public void edit(List<? extends BinaryNode<Token>> list) {
        for (; list.size() > 1; ) {
            mergeNodes(list.remove(0), list.remove(0));
        }
    }

    private void mergeNodes(BinaryNode<Token> bottom, BinaryNode<Token> top) {
        top.getLeft().setParent(top.getParent());
        top.getParent().setLeft(top.getLeft());

        bottom.getRight().setParent(top);
        top.setLeft(bottom.getRight());

        bottom.setRight(top);
        top.setParent(bottom);
    }
}

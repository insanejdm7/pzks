package com.kpi.pzks.tree.edit;

import java.util.List;

/**
 * Created by insane on 05.11.14.
 */
public interface Editor<T> {

    void edit(List<? extends T> list);

}

package com.kpi.pzks.tree.edit;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;

/**
 * Created by andriiro on 05.11.14.
 */
public abstract class TokenNodeEditor
    implements Editor<BinaryNode<Token>> {}

package com.kpi.pzks.tree;

import com.kpi.pzks.token.Token;

/**
 * Created by insane on 26.10.14.
 */
public class TokenNode extends BinaryNode<Token> {
    @Override
    public String toString() {
        return element.getDescription();
    }
}

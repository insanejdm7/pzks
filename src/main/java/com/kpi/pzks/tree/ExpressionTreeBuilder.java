package com.kpi.pzks.tree;

import com.kpi.pzks.builder.Builder;
import com.kpi.pzks.token.Token;

import java.util.List;

/**
 * Created by insane on 26.10.14.
 */
public class ExpressionTreeBuilder implements Builder<TokenNode> {

    private List<Token> input;

    public ExpressionTreeBuilder(List<Token> in) {
        input = in;
    }

    @Override
    public TokenNode build() throws Exception {
        BinaryNode currentNode = new TokenNode();
        for (int index = 0; index < input.size(); index++) {
            final Token currentToken = input.get(index);
            switch (currentToken.getType()) {
                case CONST:
                case VAR:
                    createSubNode(currentToken, currentNode);
                    break;
                case OPEN_BRACE:
                    currentNode = createSubNode(null, currentNode);
                    break;
                case CLOSE_BRACE:
                    currentNode = currentNode.getParent();
                    break;
                case FUN:
                    /*create sub node as function and go down to that node*/
                    currentNode = createSubNode(currentToken, currentNode);
                    break;
                case OPERATION:
                    if (currentNode.getElement() == null) {
                        currentNode.setElement(currentToken);
                    } else {
                        BinaryNode newParent = new TokenNode();
                        newParent.setElement(currentToken);
                        if (currentNode.getParent() == null) {
                            currentNode.setParent(newParent);
                            newParent.setLeft(currentNode);
                        } else {
                            newParent.setLeft(currentNode);
                            if (currentNode.getParent().getLeft() == currentNode) {
                                currentNode.getParent().setLeft(newParent);
                            } else {
                                currentNode.getParent().setRight(newParent);
                            }
                            newParent.setParent(currentNode.getParent());
                            currentNode.setParent(newParent);
                        }
                        currentNode = newParent;
                    }
                    break;
            }
        }
        /*find root*/
        for (; currentNode.getParent() != null; ) {
            currentNode = currentNode.getParent();
        }
        return (TokenNode) currentNode;
    }

    private BinaryNode createSubNode(Token token, BinaryNode parent) {
        final BinaryNode subNode = new TokenNode();
        subNode.setElement(token);
        subNode.setParent(parent);
        if (parent.getLeft() == null) {
            parent.setLeft(subNode);
        } else {
            parent.setRight(subNode);
        }
        return subNode;
    }
}

package com.kpi.pzks.tree.walk;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;

/**
 * Created by andriiro on 05.11.14.
 */
public class InOrderTreeWalker
    extends Walker<BinaryNode<Token>> {

    private Walker<BinaryNode<Token>> externalWalker;

    public InOrderTreeWalker(Walker<BinaryNode<Token>> externalWalker) {
        this.externalWalker = externalWalker;
    }

    @Override
    public void walk(BinaryNode<Token> current) {
        if (current.getLeft() != null) {
            walk(current.getLeft());
        }
        externalWalker.walk(current);
        if (current.getRight() != null) {
            walk(current.getRight());
        }
    }
}

package com.kpi.pzks.tree.walk;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;
import com.kpi.pzks.tree.collect.Collector;
import com.kpi.pzks.tree.edit.Editor;

/**
 * Created by insane on 05.11.14.
 */
public class DownToUpWalker
    extends Walker<BinaryNode<Token>> {

    private Editor<BinaryNode<Token>> editor;
    private Collector<BinaryNode<Token>> collector;

    public DownToUpWalker(Collector<BinaryNode<Token>> collector, Editor<BinaryNode<Token>> editor) {
        this.collector = collector;
        this.editor = editor;
    }

    public void setEditor(Editor<BinaryNode<Token>> editor) {
        this.editor = editor;
    }

    public void setCollector(Collector<BinaryNode<Token>> collector) {
        this.collector = collector;
    }

    @Override
    public void walk(BinaryNode<Token> tokenNode) {
        BinaryNode current = tokenNode;
        for (; current.getParent() != null && current.getParent().getRight() != current;) {
            current = current.getParent();
            collector.collect(current);
        }
        collector.edit(editor);
        collector.reset();
    }
}

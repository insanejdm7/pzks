package com.kpi.pzks.tree.walk;

import com.kpi.pzks.tree.BinaryNode;

/**
 * Created by insane on 05.11.14.
 */
public abstract class Walker<T extends BinaryNode<?>> {

    public abstract void walk(T t);

}

package com.kpi.pzks.tree.walk;

import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by andriiro on 05.11.14.
 */
public class PostOrderTreeWalker
        extends Walker<BinaryNode<Token>> {

    private Set<BinaryNode<Token>> excludedNodes;

    public PostOrderTreeWalker() {
        excludedNodes = new HashSet<>();
    }

    public Set<BinaryNode<Token>> getExcludedNodes() {
        return excludedNodes;
    }

    @Override
    public void walk(BinaryNode<Token> current) {
        if (current.getLeft() != null) {
            walk(current.getLeft());
        }
        if (current.getRight() != null) {
            walk(current.getRight());
        }
        if (current.getLeft() != null && current.getLeft().getLeft() == null && current.getLeft().getRight() == null && current.getRight() != null && current.getRight().getLeft() == null && current.getRight().getRight() == null && !excludedNodes.contains(current.getRight()) && !excludedNodes.contains(current.getLeft())) {
            StringBuilder sb = new StringBuilder("(");
            sb.append(current.getLeft().getElement().getDescription());
            sb.append(current.getElement().getDescription());
            sb.append(current.getRight().getElement().getDescription());
            sb.append(")");
            current.getElement().setDescription(sb.toString());
            current.setRight(null);
            current.setLeft(null);
            excludedNodes.add(current);
        }
    }
}

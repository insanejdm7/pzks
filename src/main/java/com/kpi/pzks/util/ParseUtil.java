package com.kpi.pzks.util;

import com.kpi.pzks.token.Token;

import java.util.List;

/**
 * Created by insane on 23.09.14.
 */
public class ParseUtil {

    public static String tokensToString(List<Token> list) {
        final StringBuilder result = new StringBuilder();
        list.stream().forEach(token -> result.append(token.getDescription()));
        return result.toString();
    }

}

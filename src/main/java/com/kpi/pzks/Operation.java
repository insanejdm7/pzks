package com.kpi.pzks;

/**
 * Created by insane on 31.10.14.
 */
public enum Operation {

    MINUS("-", 1),
    PLUS("+", 2),
    MUL("*", 3),
    DIVIDE("/", 4),;

    private final String str;
    private final Integer priority;

    private Operation(String str, int priority) {
        this.str = str;
        this.priority = priority;
    }

    public Integer getPriority() {
        return priority;
    }

    public static Operation getOperation(String str) {
        for (Operation operation : values()) {
            if (operation.str.equals(str)) {
                return operation;
            }
        }
        throw new IllegalArgumentException("Unknown operation " + str);
    }
}

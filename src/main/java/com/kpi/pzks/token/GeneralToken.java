package com.kpi.pzks.token;

import com.kpi.pzks.State;

/**
 * Created by insane on 23.09.14.
 */
public class GeneralToken implements Token {

    private State state;
    private String representation;
    private long startPos;

    public GeneralToken(State state, String representation, long startPos) {
        if (state == null) {
            throw new IllegalArgumentException("State can't be bull");
        }
        if (representation == null) {
            throw new IllegalArgumentException("Representation of token can't be null");
        }
        if (startPos < 0) {
            throw new IllegalArgumentException("Token start pos can't be less than 0");
        }
        this.state = state;
        this.representation = representation;
        this.startPos = startPos;
    }

    @Override
    public State getType() {
        return state;
    }

    @Override
    public String getDescription() {
        return representation;
    }

    @Override
    public long getPos() {
        return startPos;
    }

    @Override
    public void setType(State type) {
        state = type;
    }

    @Override
    public void setDescription(String description) {
        representation = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeneralToken that = (GeneralToken) o;

        if (startPos != that.startPos) return false;
        if (!representation.equals(that.representation)) return false;
        if (state != that.state) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = state.hashCode();
        result = 31 * result + representation.hashCode();
        result = 31 * result + (int) (startPos ^ (startPos >>> 32));
        return result;
    }

    @Override
    public String toString() {
//        return new StringBuilder(String.valueOf(startPos)).append('\t').append(state.name()).append('\t').append(representation).toString();
        return representation;
    }
}

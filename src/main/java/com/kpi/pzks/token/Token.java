package com.kpi.pzks.token;

import com.kpi.pzks.State;

/**
 * Created by insane on 23.09.14.
 */
public interface Token {

    State getType();

    String getDescription();

    long getPos();

    void setType(State type);

    void setDescription(String description);

}

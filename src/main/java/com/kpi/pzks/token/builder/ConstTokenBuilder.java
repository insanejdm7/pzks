package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.exception.lexical.SecondDotException;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;

import java.io.PushbackInputStream;

/**
 * Created by insane on 24.09.14.
 */
public class ConstTokenBuilder extends AbstractTokenBuilder {

    private boolean containsDot;
    private long pos;

    public ConstTokenBuilder(PushbackInputStream stream, long startPos) {
        super(stream, startPos);
        pos = startPos;
    }

    @Override
    public State getType() {
        return State.CONST;
    }

    @Override
    public Token build() throws Exception {
        int b;
        for (; stream.available() > 0; ) {
            b = stream.read();
            if (State.CONST.isStart(b)) {
                consume(b);
                continue;
            } else if (b == 46) {
                if (containsDot) {
                    throw new SecondDotException(sb.toString(), pos);
                }
                consume(b);
                containsDot = true;
                continue;
            }
            stream.unread(b);
            return new GeneralToken(getType(), sb.toString(), startPos);
        }
        return new GeneralToken(getType(), sb.toString(), startPos);
    }

    private void consume(int b) {
        sb.append(Character.toChars(b));
        pos++;
    }
}

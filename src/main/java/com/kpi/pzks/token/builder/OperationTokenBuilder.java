package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;

import java.io.PushbackInputStream;

/**
 * Created by insane on 24.09.14.
 */
public class OperationTokenBuilder extends AbstractTokenBuilder {
    public OperationTokenBuilder(PushbackInputStream stream, long startPos) {
        super(stream, startPos);
    }

    @Override
    public State getType() {
        return State.OPERATION;
    }

    @Override
    public Token build() throws Exception {
        return new GeneralToken(getType(), String.valueOf(Character.toChars(stream.read())), startPos);
    }
}

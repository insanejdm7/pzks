package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.builder.Builder;
import com.kpi.pzks.token.Token;

import java.io.PushbackInputStream;

/**
 * Created by insane on 23.09.14.
 */
public abstract class AbstractTokenBuilder implements Builder<Token> {

    protected final long startPos;

    protected StringBuilder sb;

    protected final PushbackInputStream stream;

    public AbstractTokenBuilder(PushbackInputStream stream, long startPos) {
        this.stream = stream;
        sb = new StringBuilder();
        this.startPos = startPos;
    }

    public long getStartPos() {
        return startPos;
    }

    public abstract State getType();

}

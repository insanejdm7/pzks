package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;

import java.io.PushbackInputStream;

/**
 * Created by insane on 23.09.14.
 */
public class VariableTokenBuilder extends AbstractTokenBuilder {

    public VariableTokenBuilder(PushbackInputStream stream, long startPos) {
        super(stream, startPos);
    }

    @Override
    public State getType() {
        return State.VAR;
    }

    @Override
    public Token build() throws Exception {
        int b;
        for (; stream.available() > 0; ) {
            b = stream.read();
            if (State.VAR.isStart(b) || State.CONST.isStart(b)) {
                consume(b);
                continue;
            }
            stream.unread(b);
            return new GeneralToken(getType(), sb.toString(), startPos);
        }
        return new GeneralToken(getType(), sb.toString(), startPos);
    }

    private void consume(int b) {
        sb.append(Character.toChars(b));
    }


}

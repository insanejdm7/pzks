package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.token.GeneralToken;
import com.kpi.pzks.token.Token;

import java.io.PushbackInputStream;

/**
 * Created by insane on 24.09.14.
 */
public class OpenBraceTokenBuilder extends AbstractTokenBuilder {

    public OpenBraceTokenBuilder(PushbackInputStream stream, long startPos) {
        super(stream, startPos);
    }

    @Override
    public State getType() {
        return State.OPEN_BRACE;
    }

    @Override
    public Token build() throws Exception {
        stream.read();
        return new GeneralToken(State.OPEN_BRACE, "(", startPos);
    }
}

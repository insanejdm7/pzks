package com.kpi.pzks.optimize;

import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.analyze.syntax.SyntaxAnalyzer;
import com.kpi.pzks.builder.Builder;
import com.kpi.pzks.optimization.DivideAndMinusOptimizer;
import com.kpi.pzks.optimization.ExtraBraceAdder;
import com.kpi.pzks.optimization.TokenListOptimizer;
import com.kpi.pzks.optimization.UnaryOptimizer;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.ExpressionTreeBuilder;
import com.kpi.pzks.tree.TokenNode;
import com.kpi.pzks.util.ParseUtil;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by andriiro on 03.11.14.
 */
public class ExtraBraceAdderTest {

    @Test
    public void simpleTest1() {
        try {
            String test = "(a+b)/c;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("((a+b)/c)", ParseUtil.tokensToString(tokens));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest2() {
        try {
            String test = "a+b/c;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("(a+(b/c))", ParseUtil.tokensToString(tokens));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest3() {
        try {
            String test = "a/b+f-(c+d)*e-a*c;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("((((a/b)+f)-((c+d)*e))-(a*c))", ParseUtil.tokensToString(tokens));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest4() {
        try {
            String test = "first+(A+B/C+K1*(SOUP+V*M/K+L1)+last)+D+G+C+h*(J+K/L2-N+L3);";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("(((((first+(((A+(B/C))+(K1*((SOUP+(V*(M/K)))+L1)))+last))+D)+G)+C)+(h*((J+(K/L2))-N+L3)))", ParseUtil.tokensToString(tokens));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest5() {
        try {
            String test = "SOUP+V*M/K+L1;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("((SOUP+(V*(M/K)))+L1)", ParseUtil.tokensToString(tokens));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest6() {
        try {
            String test = "a+(B*l+c)+d*q*w*e-t/r+u*i*o*p/h/j+k+l/n/m;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("(((a+((B*l)+c))+(((d*q)*w)*e))-((((t/r)+(((u*i)*o)*(p/(h*j))))+k)+(l/(n*m))))", ParseUtil.tokensToString(optimizer.optimize()));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest7() {
        try {
            String test = "35+4*2/(1-5)+8*36*((31-54)+(23+25));";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            assertEquals("((35+(4*(2/(1-5))))+((8*36)*((31-54)+(23+25))))", ParseUtil.tokensToString(tokens));
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            builder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }
}

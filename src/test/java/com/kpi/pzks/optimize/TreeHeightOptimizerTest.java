package com.kpi.pzks.optimize;

import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.analyze.syntax.SyntaxAnalyzer;
import com.kpi.pzks.builder.Builder;
import com.kpi.pzks.optimization.*;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.tree.BinaryNode;
import com.kpi.pzks.tree.ExpressionTreeBuilder;
import com.kpi.pzks.tree.TokenNode;
import com.kpi.pzks.tree.collect.Collector;
import com.kpi.pzks.tree.collect.MulHangingCollector;
import com.kpi.pzks.tree.collect.PlusCollector;
import com.kpi.pzks.tree.collect.PlusHangingCollector;
import com.kpi.pzks.tree.edit.Editor;
import com.kpi.pzks.tree.edit.MulEditor;
import com.kpi.pzks.tree.edit.PlusEditor;
import com.kpi.pzks.tree.edit.PlusHangingEditor;
import com.kpi.pzks.tree.walk.DownToUpWalker;
import com.kpi.pzks.tree.walk.InOrderTreeWalker;
import com.kpi.pzks.tree.walk.PostOrderTreeWalker;
import com.kpi.pzks.tree.walk.Walker;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 05.11.14.
 */
public class TreeHeightOptimizerTest {

    @Test
    public void simpleTest1() {
        try {
            String test = "a+b+(1+2+3+4)+c+d;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            // TokenNodeOptimizer nodeOptimizer = new TreeHeightOptimizer(builder.build());
            // nodeOptimizer.optimize();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest2() {
        try {
            String test = "a+b+t+1+2+c+v+q;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            Editor<BinaryNode<Token>> editor = new PlusHangingEditor();
            Collector<BinaryNode<Token>> collector = new PlusHangingCollector();
            DownToUpWalker downToUpWalker = new DownToUpWalker(collector, editor);
            Walker<BinaryNode<Token>> inOderWalker = new InOrderTreeWalker(downToUpWalker);
            TokenNodeOptimizer nodeOptimizer = new TreeHeightOptimizer(builder.build(), inOderWalker);
            nodeOptimizer = new TreeHeightOptimizer(nodeOptimizer.optimize(), inOderWalker);
            downToUpWalker.setCollector(new PlusCollector());
            downToUpWalker.setEditor(new PlusEditor());
            nodeOptimizer.optimize();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest3() {
        try {
            String test = "2-a/b/t/1-p*(q-w-e-b)-r*3-x;";
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(test);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), new HashSet<>());
            syntaxAnalyzer.process();
            TokenListOptimizer optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            optimizer = new UnaryOptimizer(optimizer.optimize());
            optimizer = new ExtraBraceAdder(optimizer.optimize());
            List<Token> tokens = optimizer.optimize();
            Builder<TokenNode> builder = new ExpressionTreeBuilder(tokens);
            Editor<BinaryNode<Token>> editor = new PlusHangingEditor();
            Collector<BinaryNode<Token>> collector = new PlusHangingCollector();
            DownToUpWalker downToUpWalker = new DownToUpWalker(collector, editor);
            Walker<BinaryNode<Token>> inOderWalker = new InOrderTreeWalker(downToUpWalker);
            TokenNodeOptimizer nodeOptimizer = new TreeHeightOptimizer(builder.build(), inOderWalker);
            nodeOptimizer = new TreeHeightOptimizer(nodeOptimizer.optimize(), inOderWalker);
            downToUpWalker.setCollector(new MulHangingCollector());
            downToUpWalker.setEditor(new MulEditor());
            BinaryNode<Token> node = nodeOptimizer.optimize();
            while (node.getLeft().getElement().getDescription().length() < 2) {
                PostOrderTreeWalker postOrderTreeWalker = new PostOrderTreeWalker();
                postOrderTreeWalker.walk(node);
                System.out.println(postOrderTreeWalker.getExcludedNodes());
            }
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }
}

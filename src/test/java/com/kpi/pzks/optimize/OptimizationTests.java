package com.kpi.pzks.optimize;

import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.analyze.syntax.SyntaxAnalyzer;
import com.kpi.pzks.optimization.DivideAndMinusOptimizer;
import com.kpi.pzks.optimization.TokenListOptimizer;
import com.kpi.pzks.optimization.UnaryOptimizer;
import com.kpi.pzks.token.Token;
import com.kpi.pzks.util.ParseUtil;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * Created by insane on 18.10.14.
 */
public class OptimizationTests {

    private SyntaxAnalyzer syntaxAnalyzer;
    private LexicalAnalyzer lexicalAnalyzer;
    private TokenListOptimizer optimizer;

    @Test
    public void allCasesTest() {
        try {
            final String str = "a+b-c/v+b/n/m-e+a/q/w/e/r+2+a/b/(c/(d/b))-a-b+c-v-b-n-m+z+x-s-n-m*h/v/v-q-w-e-r-t-(-a-s-(c-(g-(-h))));";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "a+b-c/v+b/(n*m)-e+a/(q*w*e*r)+2+a/(b*(c/(d/b)))-(a+b)+c-(v+b+n+m)+z+x-(s+n+m*h/(v*v)+q+w+e+r+t+(-a-(s+(c-(g-(-h))))));", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void cornerCase1Test() {
        try {
            final String str = "c-a-b;";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "c-(a+b);", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void cornerCase2Test() {
        try {
            final String str = "a/b/(b/c)/s/(a+(c/m))/(a/v/b)/((z/x/v+a/s/d/(z/x/c/v/(a+b/c))));";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "a/(b*(b/c)*s*(a+(c/m))*(a/(v*b))*((z/(x*v)+a/(s*d*(z/(x*c*v*(a+b/c)))))));", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void cornerCase3Test() {
        try {
            final String str = "a-c-v-(a+b-c-v-b/c)-z/x/c/(a-c-b-m/a/e/r/t/(q+b+n/s)-a-b-n+n/m/(q/z))+(a-z)+(a-v-b)+(a-v-m-t/y/u/i/o+(t/u/u*y*i/o/p));";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "a-(c+v+(a+b-(c+v+b/c))+z/(x*c*(a-(c+b+m/(a*e*r*t*(q+b+n/s))+a+b+n)+n/(m*(q/z)))))+(a-z)+(a-(v+b))+(a-(v+m+t/(y*u*i*o))+(t/(u*u)*y*i/(o*p)));", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.toString(), false);
        }
    }

    @Test
    public void cornerCase4Test() {
        try {
            final String str = "c-a-b/v-n-m/(c-b)-b;";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "c-(a+b/v+n+m/(c-b)+b);", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.toString(), false);
        }
    }

    @Test
    public void cases1Test() {
        try {
            final String str = "a+b-c/v+b/n/m-e;";//-e+a/q/w/e/r+2+a/b/(c/(d/b))-a-b+c-v-b-n-m+z+x-s-n-m*h/v/v-q-w-e-r-t-(-a-s-(c-(g-(-h))));";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "a+b-c/v+b/(n*m)-e;", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void cases2Test() {
        try {
            final String str = "a-c-v-(a+b-c-v-b/c)-z/x/c;";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "a-(c+v+(a+b-(c+v+b/c))+z/(x*c));", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.toString(), false);
        }
    }

    @Test
    public void cases3Test() {
        try {
            final String str = "(t/u/u*y*i/o/p);";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            Assert.assertEquals("result", "(t/(u*u)*y*i/(o*p));", ParseUtil.tokensToString(optimizer.optimize()));
        } catch (Exception e) {
            TestCase.assertTrue(e.toString(), false);
        }
    }

    @Test
    public void unaryTest1() {
        try {
            final String str = "a--b;";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new UnaryOptimizer(syntaxAnalyzer.getTokens());
            List<Token> list = optimizer.optimize();
            Assert.assertEquals(5, list.size());
            Assert.assertEquals("-b", list.get(3).getDescription());
        } catch (Exception e) {
            TestCase.assertTrue(e.toString(), false);
        }
    }

    @Test
    public void unaryTest2() {
        try {
            final String str = "(a-b)-(x-c);";
            lexicalAnalyzer = new LexicalAnalyzer(str);
            lexicalAnalyzer.process();
            syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            optimizer = new UnaryOptimizer(syntaxAnalyzer.getTokens());
            List<Token> list = optimizer.optimize();
            Assert.assertEquals(syntaxAnalyzer.getTokens(), list);
        } catch (Exception e) {
            TestCase.assertTrue(e.toString(), false);
        }
    }
}

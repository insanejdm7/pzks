package com.kpi.pzks.tree;

import com.google.common.collect.Sets;
import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.analyze.syntax.SyntaxAnalyzer;
import com.kpi.pzks.optimization.DivideAndMinusOptimizer;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 26.10.14.
 */
public class ExpressionTreeBuilderTest {

    @Test
    public void simpleTest1() {
        final String expression = "a+(b+c+(s+(v+b)))+d;";
        try {
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(expression);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            DivideAndMinusOptimizer devideAndMinusOptimizator = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            ExpressionTreeBuilder expressionTreeBuilder = new ExpressionTreeBuilder(devideAndMinusOptimizator.optimize());
            expressionTreeBuilder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest2() {
        final String expression = "q/c/v/b/n/m;";
        try {
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(expression);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Collections.emptySet());
            syntaxAnalyzer.process();
            DivideAndMinusOptimizer devideAndMinusOptimizator = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            ExpressionTreeBuilder expressionTreeBuilder = new ExpressionTreeBuilder(devideAndMinusOptimizator.optimize());
            expressionTreeBuilder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest3() {
        final String expression = "a+sin(b)+cos(c+b)+tan(sin(b/c/d));";
        try {
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(expression);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Sets.newHashSet("sin", "cos", "tan", "atan"));
            syntaxAnalyzer.process();
            DivideAndMinusOptimizer devideAndMinusOptimizator = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            ExpressionTreeBuilder expressionTreeBuilder = new ExpressionTreeBuilder(devideAndMinusOptimizator.optimize());
            expressionTreeBuilder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest4() {
        final String expression = "first+(A+B/C+K1*(SOUP+V*M/K+L1)+last)+D+G+C+h*(J+K/L2-N+L3);";
        try {
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(expression);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Sets.newHashSet("sin", "cos", "tan", "atan"));
            syntaxAnalyzer.process();
            DivideAndMinusOptimizer devideAndMinusOptimizator = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            ExpressionTreeBuilder expressionTreeBuilder = new ExpressionTreeBuilder(devideAndMinusOptimizator.optimize());
            expressionTreeBuilder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

    @Test
    public void simpleTest5() {
        final String expression = "a+b/c;";
        try {
            LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(expression);
            lexicalAnalyzer.process();
            SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Sets.newHashSet("sin", "cos", "tan", "atan"));
            syntaxAnalyzer.process();
            DivideAndMinusOptimizer devideAndMinusOptimizator = new DivideAndMinusOptimizer(syntaxAnalyzer.getTokens());
            ExpressionTreeBuilder expressionTreeBuilder = new ExpressionTreeBuilder(devideAndMinusOptimizator.optimize());
            expressionTreeBuilder.build();
        } catch (Exception e) {
            assertTrue(e.toString(), false);
        }
    }

}

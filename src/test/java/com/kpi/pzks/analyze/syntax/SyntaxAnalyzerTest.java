package com.kpi.pzks.analyze.syntax;

import com.google.common.collect.Sets;
import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.exception.syntax.MissingTokenException;
import com.kpi.pzks.exception.syntax.UnexpectedTokenException;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 25.09.14.
 */
public class SyntaxAnalyzerTest {

    @Test
    public void alotOfSuccessTests() {
        try {
            test("");
            test(" ; ");
            test(" a; ");
            test(" 2; ");
            test(" a+b; ");
            test(" 2+3.14; ");
            test(" ((2+3.14)); ");
            test(" (2+3.14)+((1+bn)); ");
            test(" (2+3.14)+(1+bn)*3/      yu-(as-cv+ew*op/3); ");
            test("(sin((a)) + (cos(b)) - sin / cos * tan((b-c)+v/(n*io)));");
            test("sin;");
            test("sin + cos;");
            test("sin(a+cos(b-tan(c/sin(cos(tan*3.14)))));");
            test("-1;");
            test("-1+2;");
            /**less pain for future*/
//            test("- (s+-(c) + (- sin(-tan(x /-s+-j3) )) );");
            test("(sin((a)) + (cos(b)) - sin / cos * tan((b-c)+v/(n*io)));");
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test(expected = UnexpectedTokenException.class)
    public void bracesFailTest1()
            throws Exception {
        test("())");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void bracesFailTest2()
            throws Exception {
        test("()))");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void bracesFailTest3()
            throws Exception {
        test("))");
    }

    @Test(expected = MissingTokenException.class)
    public void bracesFailTest4()
            throws Exception {
        test("(a+z)+(c+b)+(n-z*(g/m))+(");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void bracesFailTest5()
            throws Exception {
        test(")(");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void unaryFailTest1()
            throws Exception {
        test("-");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void unaryFailTest2()
            throws Exception {
        test("--a");
    }

    @Test(expected = MissingTokenException.class)
    public void unaryFailTest3()
            throws Exception {
        test("-a-");
    }

    @Test(expected = MissingTokenException.class)
    public void unaryFailTest4()
            throws Exception {
        test("-a-b");
    }

    @Test(expected = MissingTokenException.class)
    public void unaryFailTest5()
            throws Exception {
        test("sin");
    }

    @Test(expected = MissingTokenException.class)
    public void failTest1()
            throws Exception {
        test("-a+");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void failTest2()
            throws Exception {
        test("sin()");
    }

    @Test(expected = MissingTokenException.class)
    public void failTest3()
            throws Exception {
        test("sin(a--cos(b))");
    }

    @Test(expected = UnexpectedTokenException.class)
    public void failTest4()
            throws Exception {
        test("-a++v");
    }

    private void test(String str)
            throws Exception {
        LexicalAnalyzer lexicalAnalyzer = new LexicalAnalyzer(str);
        lexicalAnalyzer.process();
        SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(lexicalAnalyzer.getTokens(), Sets.newHashSet("sin", "cos", "tan", "atan"));
        syntaxAnalyzer.process();
    }
}

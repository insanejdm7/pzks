package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.exception.lexical.SecondDotException;
import com.kpi.pzks.token.Token;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 24.09.14.
 */
public class CloseBraceBuilderTest extends AbstractBuilderTest {

    @Test
    public void twoBracesTest() {
        final String str = "( ))";
        lexicalAnalyzer = new LexicalAnalyzer(str);
        try {
            lexicalAnalyzer.process();
            final List<Token> list = lexicalAnalyzer.getTokens();
            list.remove(0);
            assertEquals(3, list.size());
            assertEquals(State.OPEN_BRACE, list.get(0).getType());
            assertEquals(State.CLOSE_BRACE, list.get(1).getType());
            assertEquals(State.CLOSE_BRACE, list.get(2).getType());
            assertEquals(0, list.get(0).getPos());
            assertEquals(2, list.get(1).getPos());
            assertEquals(3, list.get(2).getPos());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void closeBraceVarConstTest() {
        final String str = "()re43 90.1)";
        lexicalAnalyzer = new LexicalAnalyzer(str);
        try {
            lexicalAnalyzer.process();
            final List<Token> list = lexicalAnalyzer.getTokens();
            list.remove(0);
            assertEquals(5, list.size());
            assertEquals(State.OPEN_BRACE, list.get(0).getType());
            assertEquals(State.CLOSE_BRACE, list.get(1).getType());
            assertEquals(State.VAR, list.get(2).getType());
            assertEquals(State.CONST, list.get(3).getType());
            assertEquals(State.CLOSE_BRACE, list.get(4).getType());

            assertEquals("(", list.get(0).getDescription());
            assertEquals(")", list.get(1).getDescription());
            assertEquals("re43", list.get(2).getDescription());
            assertEquals("90.1", list.get(3).getDescription());
            assertEquals(")", list.get(4).getDescription());

            assertEquals(0, list.get(0).getPos());
            assertEquals(1, list.get(1).getPos());
            assertEquals(2, list.get(2).getPos());
            assertEquals(7, list.get(3).getPos());
            assertEquals(11, list.get(4).getPos());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void openBraceVarSecondDotTest() {
        final String str = ")re43 90.1.1";
        lexicalAnalyzer = new LexicalAnalyzer(str);
        try {
            lexicalAnalyzer.process();
        } catch (SecondDotException e) {
            final List<Token> list = lexicalAnalyzer.getTokens();
            list.remove(0);
            assertEquals(2, list.size());
            assertEquals(State.CLOSE_BRACE, list.get(0).getType());
            assertEquals(State.VAR, list.get(1).getType());

            assertEquals(")", list.get(0).getDescription());
            assertEquals("re43", list.get(1).getDescription());

            assertEquals(0, list.get(0).getPos());
            assertEquals(1, list.get(1).getPos());

            assertEquals("90.1", e.getToken());
            assertEquals(10, e.getPos());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

}

package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.exception.lexical.SecondDotException;
import com.kpi.pzks.token.Token;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 24.09.14.
 */
public class ConstTokenBuilderTest extends AbstractBuilderTest {

    @Test
    public void twoConstantsTest() {
        final String line = "12  3.4 ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
            List<Token> tokens = lexicalAnalyzer.getTokens();
            tokens.remove(0);
            assertEquals(2, tokens.size());
            assertEquals("12", tokens.get(0).getDescription());
            assertEquals("3.4", tokens.get(1).getDescription());
            assertEquals(0, tokens.get(0).getPos());
            assertEquals(4, tokens.get(1).getPos());
            assertEquals(State.CONST, tokens.get(0).getType());
            assertEquals(State.CONST, tokens.get(1).getType());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void secondDotTest() {
        final String line = " 12 3.4.5 ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
        } catch (SecondDotException e) {
            assertEquals("3.4", e.getToken());
            assertEquals(7, e.getPos());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void constVarTest() {
        final String line = "  ab  3.4 ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
            List<Token> tokens = lexicalAnalyzer.getTokens();
            tokens.remove(0);
            assertEquals(2, tokens.size());
            assertEquals("ab", tokens.get(0).getDescription());
            assertEquals("3.4", tokens.get(1).getDescription());
            assertEquals(2, tokens.get(0).getPos());
            assertEquals(6, tokens.get(1).getPos());
            assertEquals(State.VAR, tokens.get(0).getType());
            assertEquals(State.CONST, tokens.get(1).getType());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

}

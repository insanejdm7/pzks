package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.exception.lexical.UnexpectedCharacterException;
import com.kpi.pzks.token.Token;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 23.09.14.
 */
public class VariableTokenBuilderTest extends AbstractBuilderTest {

    @Test
    public void emptyLineTest() {
        final String line = "   ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
            lexicalAnalyzer.getTokens().remove(0);
            assertEquals(0, lexicalAnalyzer.getTokens().size());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void unexpectedCharAtEmptyLineTest() {
        final String line = " ^  ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
        } catch (UnexpectedCharacterException e) {
            assertEquals('^', e.getC());
            assertEquals(1, e.getPos());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void unexpectedCharWithVarsTest() {
        final String line = " a b ^  ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
        } catch (UnexpectedCharacterException e) {
            List<Token> tokens = lexicalAnalyzer.getTokens();
            tokens.remove(0);
            assertEquals(2, tokens.size());
            assertEquals("a", tokens.get(0).getDescription());
            assertEquals("b", tokens.get(1).getDescription());
            assertEquals(1, tokens.get(0).getPos());
            assertEquals(3, tokens.get(1).getPos());
            assertEquals(State.VAR, tokens.get(0).getType());
            assertEquals(State.VAR, tokens.get(1).getType());
            assertEquals('^', e.getC());
            assertEquals(5, e.getPos());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void twoVariablesTest() {
        final String line = " abc sdf ";
        lexicalAnalyzer = new LexicalAnalyzer(line);
        try {
            lexicalAnalyzer.process();
            List<Token> tokens = lexicalAnalyzer.getTokens();
            tokens.remove(0);
            assertEquals(2, tokens.size());
            assertEquals("abc", tokens.get(0).getDescription());
            assertEquals("sdf", tokens.get(1).getDescription());
            assertEquals(1, tokens.get(0).getPos());
            assertEquals(5, tokens.get(1).getPos());
            assertEquals(State.VAR, tokens.get(0).getType());
            assertEquals(State.VAR, tokens.get(1).getType());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }
}

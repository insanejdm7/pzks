package com.kpi.pzks.token.builder;

import com.kpi.pzks.State;
import com.kpi.pzks.analyze.lexical.LexicalAnalyzer;
import com.kpi.pzks.token.Token;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 24.09.14.
 */
public class EndTokenBuilderTest extends AbstractBuilderTest {

    @Test
    public void endTest() {
        final String str = ";;;;";
        lexicalAnalyzer = new LexicalAnalyzer(str);
        try {
            lexicalAnalyzer.process();
            final List<Token> list = lexicalAnalyzer.getTokens();
            list.remove(0);
            assertEquals(4, list.size());
            assertEquals(State.END, list.get(0).getType());
            assertEquals(State.END, list.get(1).getType());
            assertEquals(State.END, list.get(2).getType());
            assertEquals(State.END, list.get(3).getType());

            assertEquals(0, list.get(0).getPos());
            assertEquals(1, list.get(1).getPos());
            assertEquals(2, list.get(2).getPos());
            assertEquals(3, list.get(3).getPos());

            assertEquals(";", list.get(0).getDescription());
            assertEquals(";", list.get(1).getDescription());
            assertEquals(";", list.get(2).getDescription());
            assertEquals(";", list.get(3).getDescription());
        } catch (Exception e) {
            assertTrue(e.getMessage(), false);
        }
    }

}
